# README #
Web application to perform database operations.
The project prepeared for studies subject 'Databases 2'. 
Please check wiki to see detailed description.

### Summary of technologies and tools ###
* MySQL - Northwind database
* Java 8
* Spring MVC
* Hibernate
* JSP
* Maven
* Apache Tomcat