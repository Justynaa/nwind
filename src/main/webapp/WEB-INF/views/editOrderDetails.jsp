<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Szczegóły zamówienia</title>

<style>
.error {
	color: #ff0000;
}
</style>

</head>

<body>

	<form:form method="POST" modelAttribute="orderProductForm">

		<table>
			<tr>
				<td><label for="products.productId">ID produktu: </label></td>
				<td><form:input path="products.productId"
						id="products.productId" /></td>
				<td><form:errors path="products.productId" cssClass="error" /></td>
				<td><c:choose>
						<c:when test="${errMsg}">
							<c:out value="Produkt został wycofany." />
						</c:when>
					</c:choose>
			</tr>

			<tr>
				<td><label for="unitPrice">Cena za jednostkę: </label></td>
				<td><form:input path="unitPrice" id="unitPrice" /></td>
				<td><form:errors path="unitPrice" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="quantity">Ilość: </label></td>
				<td><form:input path="quantity" id="quantity" /></td>
				<td><form:errors path="quantity" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="discount">Zniżka</label></td>
				<td><form:input path="discount" id="discount" /></td>
				<td><form:errors path="discount" cssClass="error" /></td>
			</tr>



			<tr>
				<td colspan="3"><c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Update" />
						</c:when>
						<c:otherwise>
							<input type="submit" value="Dodaj szczegóły zamówienia" />
						</c:otherwise>
					</c:choose></td>
			</tr>


		</table>
	</form:form>

	<br />
	<br />
</body>
</html>