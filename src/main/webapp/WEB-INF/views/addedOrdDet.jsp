<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Dodawanie szczegółów zamówienia</title>
	</head>
	
	<body>
		Informacja
		<br/>Nr zamówienia: ${orderId}
		<br/>${info}
	    <br/>
	    <br/>
	    Dodaj kolejne <a href="<c:url value='/userPanel/orderDetails/${orderId}' />">szczegóły</a>	   
	    Zakończ dodawanie <a href="<c:url value='/userPanel' />">szczegółów</a>
	</body>
</html>