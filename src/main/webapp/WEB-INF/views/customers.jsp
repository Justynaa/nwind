<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Klienci</title>
</head>
<body>
	<div>
		<form:form method="POST" modelAttribute="searchTerm">
      Proste wyszukiwanie
      </br>
      Podaj kraj(e) i/lub nazwę firmy: <input type="text"
				name="searchTerm" />
			<br />
			<input type="reset" />
			<input type="submit" />
			<br>
			<br>
			<a href="<c:url value='/userPanel' />">Wróć do panelu użytkownika</a>
			<br>
			<br>
		</form:form>
	</div>
	<div align="center">
		<h1>Lista klientów</h1>
		<table border="1">
			<th>ID</th>
			<th>Nazwa firmy</th>
			<th>Osoba kontaktowa</th>
			<th>Tytuł</th>
			<th>Adres</th>
			<th>Miasto</th>
			<th>Region</th>
			<th>Kod pocztowy</th>
			<th>Kraj</th>
			<th>Telefon</th>
			<th>Fax</th>
			<th>Edytuj</th>
			<th>Usuń</th>

			<c:forEach var="customers" items="${customersList}">
				<tr>
					<td>${customers.customerId}</td>
					<td>${customers.companyName}</td>
					<td>${customers.contactName}</td>
					<td>${customers.contactTitle}</td>
					<td>${customers.address}</td>
					<td>${customers.city}</td>
					<td>${customers.region}</td>
					<td>${customers.postalCode}</td>
					<td>${customers.country}</td>
					<td>${customers.phone}</td>
					<td>${customers.fax}</td>
					<td><a
						href="<c:url value='/userPanel/customers/edit/${customers.customerId}' />">Edytuj</a></td>
					<td><a
						href="<c:url value='/userPanel/customers/delete/${customers.customerId}' />">Usuń</a></td>
				</tr>
			</c:forEach>
		</table>
		<c:choose>
			<c:when test="${index == 1}">
				<a href="<c:url value='/userPanel/customers/list/${index+1}' />">Następna</a>
			</c:when>
			<c:when test="${index > 1}">
				<a href="<c:url value='/userPanel/customers/list/${index-1}' />">Poprzednia</a>
				<a href="<c:url value='/userPanel/customers/list/${index+1}' />">Następna</a>
			</c:when>
		</c:choose>
	</div>
</body>
</html>