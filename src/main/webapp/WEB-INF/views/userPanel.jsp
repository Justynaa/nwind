<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>
<html>
	<head>
		<title>Panel użytkownika</title>
	</head>
	<body>
		<h1>Panel użytkownika</h1>
		<h2>Witaj! <security:authentication property="name" /></h2>
		<security:authentication property="authorities" var="authorities" />
		<ul>
			<c:forEach items="${authorities}" var="authority">
			<li>${authority.authority}</li>
			</c:forEach>
		</ul>
		<ul>
		<li><a href="<c:url value="/userPanel/customers/list/1" />">Lista klientów</a></li>
		<li><a href="<c:url value="/userPanel/customers/add" />">Dodaj nowego klienta</a></li>
		<li><a href="<c:url value="/userPanel/orders/list/1" />">Lista zamówień</a></li>
		<li><a href="<c:url value="/userPanel/products/list/1" />">Lista produktów</a></li>
		<li><a href="<c:url value="/userPanel/orders/add" />">Złóż zamówienie</a></li>
		<li><a href="<c:url value="/userPanel/search" />">Wyszukiwanie</a>
		<li><a href="<c:url value="/userPanel/chooseReport" />">Raportowanie</a>
		<li><a href="<c:url value="/logout" />">Wyloguj się</a></li>
		</ul>
	</body>
</html>
