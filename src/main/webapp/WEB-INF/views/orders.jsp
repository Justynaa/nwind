<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Zamówienia</title>
</head>
<body>
<a href="<c:url value='/userPanel' />">Wróć do panelu użytkownika</a>
	<div align="center">
		<h1>Lista zamówień</h1>
		<table border="1">
			<th>ID</th>
			<th>ID klienta</th>
			<th>ID pracownika</th>
			<th>Data zamówienia</th>
			<th>Deadline</th>
			<th>Data wysyłki</th>
			<th>ID spedytora</th>
			<th>Ładunek</th>
			<th>Odbiorca</th>
			<th>Adres wysyłki</th>
			<th>Miasto</th>
			<th>Region</th>
			<th>Kod pocztowy</th>
			<th>Kraj</th>
			<th>Edytuj</th>
			<th>Szczegóły</th>
			<th>Usuń</th>

			<c:forEach var="orders" items="${ordersList}">
				<tr>
					<td>${orders.orderId}</td>
					<td>${orders.customers.customerId}</td>
					<td>${orders.employees.employeeId}</td>
					<td>${orders.orderDate}</td>
					<td>${orders.requiredDate}</td>
					<td>${orders.shippedDate}</td>
					<td>${orders.shippers.shipperId}</td>
					<td>${orders.freight}</td>
					<td>${orders.shipName}</td>
					<td>${orders.shipAddress}</td>
					<td>${orders.shipCity}</td>
					<td>${orders.shipRegion}</td>
					<td>${orders.shipPostalCode}</td>
					<td>${orders.shipCountry}</td>
					<td><a
						href="<c:url value='/userPanel/orders/edit/${orders.orderId}' />">Edytuj</a></td>
					<td><a
						href="<c:url value='/userPanel/orderDetails/list/${orders.orderId}' />">Pokaż</a>
					<td><a
						href="<c:url value='/userPanel/orders/delete/${orders.orderId}' />">Usuń</a></td>
				</tr>
			</c:forEach>
		</table>
		<c:choose>
			<c:when test="${index == 1}">
				<a href="<c:url value='/userPanel/orders/list/${index+1}' />">Następna</a>
			</c:when>
			<c:when test="${index > 1}">
				<a href="<c:url value='/userPanel/orders/list/${index-1}' />">Poprzednia</a>
				<a href="<c:url value='/userPanel/orders/list/${index+1}' />">Następna</a>
			</c:when>
		</c:choose>
		<a href="<c:url value='/userPanel' />">Wróć do panelu użytkownika</a>
	</div>
</body>
</html>