<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Potwierdzenie operacji</title>
	</head>
	<body>
	    Informacja: ${success}
	    <br/>
	    <br/>
	    Wróć do <a href="<c:url value='/userPanel' />">panelu</a>
	     
	</body>
</html>