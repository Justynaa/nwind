<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <title>Formularz dodawania nowego produktu</title>
	 
		<style>
		    .error {
		        color: #ff0000;
		    }
		</style>
	 
	</head>
	 
	<body>
	  
	    <form:form method="POST" modelAttribute="product">
	     
	        <table>
	            <tr>
	                <td><label for="productId">ID produktu: </label> </td>
	                <td><form:input path="productId" id="productId"/></td>
	                <td><form:errors path="productId" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="productName">Nazwa produktu: </label> </td>
	                <td><form:input path="productName" id="productName"/></td>
	                <td><form:errors path="productName" cssClass="error"/></td>
	            </tr>
	         
	            <tr>
	                <td><label for="suppliers.supplierId">ID dostawcy: </label> </td>
	                <td><form:input path="suppliers.supplierId" id="suppliers.supplierId"/></td>
	                <td><form:errors path="suppliers.supplierId" cssClass="error"/></td>
	            </tr>
	     
	            <tr>
	                <td><label for="categories.categoryId">ID kategorii: </label> </td>
	                <td><form:input path="categories.categoryId" id="categories.categoryId"/></td>
	                <td><form:errors path="categories.categoryId" cssClass="error"/></td>
	            </tr>
	                 <tr>
	                <td><label for="quantityPerUnit">Ilość / jednostkę: </label> </td>
	                <td><form:input path="quantityPerUnit" id="quantityPerUnit"/></td>
	                <td><form:errors path="quantityPerUnit" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="unitPrice">Cena / jednostkę: </label> </td>
	                <td><form:input path="unitPrice" id="unitPrice"/></td>
	                <td><form:errors path="unitPrice" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="unitsInStock">UnitsInStock: </label> </td>
	                <td><form:input path="unitsInStock" id="unitsInStock"/></td>
	                <td><form:errors path="unitsInStock" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="unitsOnOrder">UnitsOnOrder: </label> </td>
	                <td><form:input path="unitsOnOrder" id="unitsOnOrder"/></td>
	                <td><form:errors path="unitsOnOrder" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="reorderLevel">ReorderLevel: </label> </td>
	                <td><form:input path="reorderLevel" id="reorderLevel"/></td>
	                <td><form:errors path="reorderLevel" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="discontinued">Discontinued: </label> </td>
	                <td><form:input path="discontinued" id="discontinued"/></td>
	                <td><form:errors path="discontinued" cssClass="error"/></td>
	            </tr>
	     
	            <tr>
	                <td colspan="3">
	                    <c:choose>
	                        <c:when test="${edit}">
	                            <input type="submit" value="Update"/>
	                        </c:when>
	                        <c:otherwise>
	                            <input type="submit" value="Dodaj produkt"/>
	                        </c:otherwise>
	                    </c:choose>
	                </td>
	            </tr>
	        </table>
	    </form:form>
	    <br/>
	    <br/>
	    <a href="<c:url value='/userPanel' />">Wróć do panelu użytkownika</a>
	</body>
</html>