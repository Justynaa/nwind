<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Szczegóły zamówienia</title>
    </head>
    <body>
        <div align="center">
            <h1>Lista szczegółów zamówienia</h1>
            <table border="1">
            	<th>Nr</th>
              	<th>Nr zamówienia</th>
              	<th>ID produktu</th>
                <th>Cena / jedn.</th>
                <th>Ilość</th>
                <th>Zniżka</th>
                 
                <c:forEach var="orderdetails" items="${orderDetailsList}" varStatus="status">
                <tr>
                    <td>${status.index + 1}</td>
                    <td>${orderdetails.orders.orderId}</td>
                    <td>${orderdetails.products.productId}</td>
                    <td>${orderdetails.unitPrice}</td>
                    <td>${orderdetails.quantity}</td>
                    <td>${orderdetails.discount}</td>
                </tr>
                </c:forEach>             
            </table>
        </div>
    </body>
</html>