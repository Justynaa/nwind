<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <title>Formularz dodawania nowego zamówienia</title>
	 
		<style>
		    .error {
		        color: #ff0000;
		    }
		</style>
	 
	</head>
	 
	<body>
	  
	    <form:form method="POST" modelAttribute="orderProduct">
	     
	        <table>
	            <tr>
	                <td><label for="customers.customerId">ID klienta: </label> </td>
	                <td><form:input path="customers.customerId" id="customers.customerId"/></td>
	                <td><form:errors path="customers.customerId" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="employees.employeeId">ID pracownika: </label> </td>
	                <td><form:input path="employees.employeeId" id="employees.employeeId"/></td>
	                <td><form:errors path="employees.employeeId" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="shippers.shipperId">ID spedytora: </label> </td>
	                <td><form:input path="shippers.shipperId" id="shippers.shipperId"/></td>
	                <td><form:errors path="shippers.shipperId" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for=orderDate>Data złożenia zamówienia: </label> </td>
	                <td><form:input path="orderDate" id="orderDate"/></td>
	                <td><form:errors path="orderDate" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="requiredDate">Deadline: </label> </td>
	                <td><form:input path="requiredDate" id="requiredDate"/></td>
	                <td><form:errors path="requiredDate" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="freight">Ładunek: </label> </td>
	                <td><form:input path="freight" id="freight"/></td>
	                <td><form:errors path="freight" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="shipName">Odbiorca: </label> </td>
	                <td><form:input path="shipName" id="shipName"/></td>
	                <td><form:errors path="shipName" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="shipAddress">Adres odbiorcy: </label> </td>
	                <td><form:input path="shipAddress" id=""/></td>
	                <td><form:errors path="shipAddress" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="shipCity">Miasto: </label> </td>
	                <td><form:input path="shipCity" id="shipCity"/></td>
	                <td><form:errors path="shipCity" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="shipRegion">Region: </label> </td>
	                <td><form:input path="shipRegion" id="shipRegion"/></td>
	                <td><form:errors path="shipRegion" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="shipPostalCode">Kod pocztowy: </label> </td>
	                <td><form:input path="shipPostalCode" id="shipPostalCode"/></td>
	                <td><form:errors path="shipPostalCode" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="shipCountry">Kraj: </label> </td>
	                <td><form:input path="shipCountry" id="shipCountry"/></td>
	                <td><form:errors path="shipCountry" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="products.productId">ID pracownika: </label> </td>
	                <td><form:input path="products.productId" id="products.productId"/></td>
	                <td><form:errors path="products.productId" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="unitPrice">Cena / jedn.: </label> </td>
	                <td><form:input path="unitPrice" id="unitPrice"/></td>
	                <td><form:errors path="unitPrice" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="quantity">Ilość: </label> </td>
	                <td><form:input path="quantity" id="quantity"/></td>
	                <td><form:errors path="quantity" cssClass="error"/></td>
	            </tr><tr>
	                <td><label for="discount">Zniżka: </label> </td>
	                <td><form:input path="discount" id="discount"/></td>
	                <td><form:errors path="discount" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td colspan="3">
	                            <input type="submit" value="Dodaj zamówienie"/>
	                </td>
	                </tr>
	            
	         </table>
	         </form:form>
	         
	         

</body>
</html>