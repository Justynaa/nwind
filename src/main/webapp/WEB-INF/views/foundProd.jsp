<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Wyniki wyszukiwania</title>
</head>
<body>
	<div align="center">
		<h1>Wyniki wyszukiwania</h1>
		<table border="1">
			<th>L.p</th>
			<th>Nr produktu</th>
			<th>Nazwa produktu</th>
			<th>Nr zamówienia</th>

			<c:forEach var="foundList" items="${foundList}" varStatus="status">
				<tr>
					<td>${status.index + 1}</td>
					<td>${foundList[0]}</td>
					<td>${foundList[1]}</td>
					<td>${foundList[2].orderId}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>