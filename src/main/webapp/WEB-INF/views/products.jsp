<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Produkty</title>
</head>
<body>
<a href="<c:url value='/userPanel' />">Wróć do panelu użytkownika</a>
	<div align="center">
		<h1>Lista produktów</h1>
		<table border="1">
			<th>ID</th>
			<th>Nazwa</th>
			<th>ID dostawcy</th>
			<th>ID kategorii</th>
			<th>Ilość / jednostkach</th>
			<th>Cena / jednostkę</th>
			<th>Ilość na magazynie (w jednostkach)</th>
			<th>UnitsOnOrder</th>
			<th>ReorderLevel</th>
			<th>Discontinued</th>
			<th>Edytuj</th>
			<th>Usuń</th>

			<c:forEach var="products" items="${productsList}">
				<tr>
					<td>${products.productId}</td>
					<td>${products.productName}</td>
					<td>${products.suppliers.supplierId}</td>
					<td>${products.categories.categoryId}</td>
					<td>${products.quantityPerUnit}</td>
					<td>${products.unitPrice}</td>
					<td>${products.unitsInStock}</td>
					<td>${products.unitsOnOrder}</td>
					<td>${products.reorderLevel}</td>
					<td>${products.discontinued}</td>
					<td><a
						href="<c:url value='/userPanel/products/edit/${products.productId}' />">Edytuj</a></td>
					<td><a
						href="<c:url value='/userPanel/products/delete/${products.productId}' />">Usuń</a></td>
				</tr>
			</c:forEach>
		</table>
		<c:choose>
			<c:when test="${index == 1}">
				<a href="<c:url value='/userPanel/products/list/${index+1}' />">Następna</a>
			</c:when>
			<c:when test="${index > 1}">
				<a href="<c:url value='/userPanel/products/list/${index-1}' />">Poprzednia</a>
			</c:when>
		</c:choose>
	</div>
</body>
</html>