<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>Formularz rejestracji nowego klienta</title>
	 
		<style>
		    .error {
		        color: #ff0000;
		    }
		</style>
	 
	</head>
	 
	<body>
	 
	    <h2>Formularz rejestracji</h2>
	  
	    <form:form method="POST" modelAttribute="customer">
	     
	        <table>
	            <tr>
	                <td><label for="customerId">Id klienta: </label> </td>
	                <td><form:input path="customerId" id="customerId"/></td>
	                <td><form:errors path="customerId" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="companyName">Nazwa klienta: </label> </td>
	                <td><form:input path="companyName" id="companyName"/></td>
	                <td><form:errors path="companyName" cssClass="error"/></td>
	            </tr>
	         
	            <tr>
	                <td><label for="contactName">Osoba kontaktowa: </label> </td>
	                <td><form:input path="contactName" id="contactName"/></td>
	                <td><form:errors path="contactName" cssClass="error"/></td>
	            </tr>
	     
	            <tr>
	                <td><label for="contactTitle">Tytuł osoby kontaktowej: </label> </td>
	                <td><form:input path="contactTitle" id="contactTitle"/></td>
	                <td><form:errors path="contactTitle" cssClass="error"/></td>
	            </tr>
	                 <tr>
	                <td><label for="address">Adres: </label> </td>
	                <td><form:input path="address" id="address"/></td>
	                <td><form:errors path="address" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="city">Miasto: </label> </td>
	                <td><form:input path="city" id="contactTitle"/></td>
	                <td><form:errors path="city" cssClass="city"/></td>
	            </tr>
	            <tr>
	                <td><label for="region">Region: </label> </td>
	                <td><form:input path="region" id="region"/></td>
	                <td><form:errors path="region" cssClass="error"/></td>
	            </tr>
	                        <tr>
	                <td><label for="postalCode">Kod pocztowy: </label> </td>
	                <td><form:input path="postalCode" id="postalCode"/></td>
	                <td><form:errors path="postalCode" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="country">Kraj: </label> </td>
	                <td><form:input path="country" id="country"/></td>
	                <td><form:errors path="country" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="phone">Telefon: </label> </td>
	                <td><form:input path="phone" id="phone"/></td>
	                <td><form:errors path="phone" cssClass="error"/></td>
	            </tr>
	            <tr>
	                <td><label for="fax">Fax: </label> </td>
	                <td><form:input path="fax" id="fax"/></td>
	                <td><form:errors path="fax" cssClass="error"/></td>
	            </tr>
	            
	     
	            <tr>
	                <td colspan="3">
	                    <c:choose>
	                        <c:when test="${edit}">
	                            <input type="submit" value="Update"/>
	                        </c:when>
	                        <c:otherwise>
	                            <input type="submit" value="Zarejestruj klienta"/>
	                        </c:otherwise>
	                    </c:choose>
	                </td>
	            </tr>
	        </table>
	    </form:form>
	    <br/>
	    <br/>
	    <a href="<c:url value='/userPanel' />">Wróć do panelu użytkownika</a>
	</body>
</html>