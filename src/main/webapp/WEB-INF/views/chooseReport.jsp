<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Raportowanie</title>
</head>
<body>
	<form:form method="POST" modelAttribute="reportModel">
		<table>
			<tr>
				<td>Klienci:</td>
			</tr>
			<tr>
				<td><form:select path="reportName">
						<form:option value="none" label="--- Wybierz ---" />
						<form:option value="totalOrdersCost"
							label="10-ciu klientów z najwyższymi sumarycznymi kwotami zamówień" />
					</form:select></td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" value="Pokaż raport" /></td>
			</tr>
		</table>
	</form:form>

	<form:form method="POST" modelAttribute="reportModel">
		<table>
			<tr>
				<td>Produkty:</td>
			</tr>
			<tr>
				<td><form:select path="reportName">
						<form:option value="none" label="--- Wybierz ---" />
						<form:option value="totalQuantityInOrders"
							label="10 produktów o największej sumarycznej ilości w zamówieniach" />
						<form:option value="mostOrdered"
							label="10 najczęściej zamawianych produktów" />
					</form:select></td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" value="Pokaż raport" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>