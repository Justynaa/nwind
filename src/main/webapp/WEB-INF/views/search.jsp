<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Złożone wyszukiwanie</title>
    </head>
    <body>
    <form:form method="POST" modelAttribute="searchModel">
	            <table>
	             <tr>
	                <td><label for="typeToSearch">Wyszukaj w: </label></td>
	                <td>
	                <form:radiobutton path="typeToSearch" value="customers"/>Klienci
	                <form:radiobutton path="typeToSearch" value="products"/>Produkty
	                <form:radiobutton path="typeToSearch" value="ordDetails"/>Szczegóły zamówień
	                </td>
	            </tr>
	            <tr>
	                <td><label for="products.productName">Nazwa produktu</label></td>
	                <td><form:input path="products.productName" id="products.productName"/></td>
	                <td><form:errors path="products.productName"/></td>
	            </tr>
	           <tr>
	                <td><label for="customers.companyName">Nazwa firmy</label></td>
	                <td><form:input path="customers.companyName" id="customers.companyName"/></td>
	                <td><form:errors path="customers.companyName"/></td>
	            </tr>
	            <tr>
	            <td><input type="reset"/></td>
      			<td><input type="submit"/></td>
      			</tr>
      </table>
    </form:form>
    </body>
</html>