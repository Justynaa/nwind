<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Raport</title>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var divContents = $("#pdf").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Raport</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>

</head>
<body>
	<div id="pdf" align="center">
		<br/>
		<table border="1">
			<c:choose>
				<c:when test="${reportName eq 'totalOrdersCost'}">
				<colgroup>
                <col width="20%">
                <col width="30%">
            </colgroup>
					<th>L.p</th>
					<th>ID klienta</th>
					<th>Nazwa firmy</th>
					<th>Całkowita kwota zamówień</th>
				</c:when>
				<c:when test="${reportName eq 'totalQuantityInOrders'}">
				<colgroup>
                <col width="20%">
                <col width="20%">
                <col width="30%">
            </colgroup>
					<th>L.p</th>
					<th>Nazwa produktu</th>
					<th>Sumaryczna ilość w zamówieniach</th>
				</c:when>
				<c:when test="${reportName eq 'mostOrdered'}">
				<colgroup>
                <col width="20%">
                <col width="20%">
            </colgroup>
					<th>L.p</th>
					<th>Nazwa produktu</th>
					<th>Liczba zamówień</th>
				</c:when>
			</c:choose>
			<tr>
				<c:forEach items="${reportList}" var="repList" varStatus="status">
					<tr>
						<td>${status.index + 1}</td>
						<c:forEach items="${repList}" var="report">
							<td>${report}</td>
						</c:forEach>
					</tr>
				</c:forEach>
			</tr>
			</table>
	</div>
	<input type="button" value="Drukuj" id="btnPrint" />
</body>
</html>