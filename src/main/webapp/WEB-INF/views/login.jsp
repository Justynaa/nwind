<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
	<head>
	<title>Logowanie</title>
	</head>
	<body>
		<h1>Logowanie</h1>
		<c:set var="loginUrl"><c:url value="/login"/></c:set>
		<form method="post" action="${loginUrl}">
		    <input type="text" name="username" />
		    <input type="password" name="password" />
		    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		    <input type="submit" value="Zaloguj" />
		</form>
		<c:if test="${not empty param.error}">
			<font color="red"> Błąd logowania. <br />
			Przyczyna: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
			</font>
		</c:if>
	</body>
</html>