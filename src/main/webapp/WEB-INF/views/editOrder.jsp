<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Formularz dodawania nowego zamówienia</title>

<style>
.error {
	color: #ff0000;
}
</style>

</head>

<body>
	<form:form method="POST" modelAttribute="order">
		<table>
			<tr>
				<c:choose>
					<c:when test="${edit}">
						<td><label for="orderId">ID zamówienia: </label></td>
						<td><form:input path="orderId" id="orderId" disabled="true" placeholder="${orderId}"/></td>
					</c:when>
					<c:otherwise>
						<form:hidden path="orderId" id="orderId" />
					</c:otherwise>
				</c:choose>
			</tr>
			<tr>
				<td><label for="customers.customerId">ID klienta: </label></td>
				<td><form:input path="customers.customerId"
						id="customers.customerId" /></td>
				<td><form:errors path="customers.customerId" cssClass="error" /></td>
			</tr>

			<tr>
				<td><label for="employees.employeeId">ID pracownika: </label></td>
				<td><form:input path="employees.employeeId"
						id="employees.employeeId" /></td>
				<td><form:errors path="employees.employeeId" cssClass="error" /></td>
			</tr>

			<tr>
				<td><label for="orderDate">Data złożenia zamówienia: </label></td>
				<td><form:input path="orderDate" id="orderDate"
						placeholder="RRRR-MM-DD" /></td>
				<td><form:errors path="orderDate" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="requiredDate">Deadline: </label></td>
				<td><form:input path="requiredDate" id="requiredDate"
						placeholder="RRRR-MM-DD" /></td>
				<td><form:errors path="requiredDate" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shippedDate">Data wysyłki: </label></td>
				<td><form:input path="shippedDate" id="shippedDate"
						placeholder="RRRR-MM-DD" /></td>
				<td><form:errors path="shippedDate" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shippers.shipperId">ID spedytora: </label></td>
				<td><form:input path="shippers.shipperId"
						id="shippers.shipperId" /></td>
				<td><form:errors path="shippers.shipperId" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="freight">Załadunek: </label></td>
				<td><form:input path="freight" id="freight" /></td>
				<td><form:errors path="freight" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shipName">Odbiorca: </label></td>
				<td><form:input path="shipName" id="shipName" /></td>
				<td><form:errors path="shipName" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shipAddress">Adres: </label></td>
				<td><form:input path="shipAddress" id="shipAddress" /></td>
				<td><form:errors path="shipAddress" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shipCity">Miasto: </label></td>
				<td><form:input path="shipCity" id="shipCity" /></td>
				<td><form:errors path="shipCity" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shipRegion">Region: </label></td>
				<td><form:input path="shipRegion" id="shipRegion" /></td>
				<td><form:errors path="shipRegion" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shipPostalCode">Kod pocztowy: </label></td>
				<td><form:input path="shipPostalCode" id="shipPostalCode" /></td>
				<td><form:errors path="shipPostalCode" cssClass="error" /></td>
			</tr>
			<tr>
				<td><label for="shipCountry">Kraj: </label></td>
				<td><form:input path="shipCountry" id="shipCountry" /></td>
				<td><form:errors path="shipCountry" cssClass="error" /></td>
			</tr>


			<tr>
				<td colspan="3"><c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Update" />
						</c:when>
						<c:otherwise>
							<input type="submit" value="Dodaj zamówienie" />
						</c:otherwise>
					</c:choose></td>
			</tr>
		</table>
	</form:form>
	<br />
	<br />
	<a href="<c:url value='/userPanel' />">Wróć do panelu użytkownika</a>
</body>
</html>