package com.database.service;

import java.util.List;

import com.database.model.Orderdetails;
import com.database.model.Orders;
import com.database.model.OrderDetailsId;

public interface OrderDetailsService {
	public List<Orderdetails> listOrderDetails(Orders order);
	public Orderdetails findOrderDetailsById(OrderDetailsId orderDetailsId);
	public void updateOrderDetails(Orderdetails orderDetails);
	public void deleteOrderDetails(OrderDetailsId orderDetailsId);
}
