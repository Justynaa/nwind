package com.database.service;

import java.util.List;

import com.database.model.SearchForm;

public interface SearchService {
	public List<Object[]> search(SearchForm searchForm);
}
