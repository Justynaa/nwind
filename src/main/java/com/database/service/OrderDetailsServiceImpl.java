package com.database.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.OrderDetailsDAO;
import com.database.model.Orderdetails;
import com.database.model.Orders;
import com.database.model.OrderDetailsId;

@Service
public class OrderDetailsServiceImpl implements OrderDetailsService {
	@Autowired
	private OrderDetailsDAO orderDetailsDAO;

	public OrderDetailsServiceImpl(OrderDetailsDAO orderDetailsDAO) {
		this.orderDetailsDAO = orderDetailsDAO;
	}

	@Override
	@Transactional
	public List<Orderdetails> listOrderDetails(Orders order) {
		return this.orderDetailsDAO.listOrderDetails(order);
	}

	@Override
	@Transactional
	public Orderdetails findOrderDetailsById(OrderDetailsId orderDetailsId) {
		return orderDetailsDAO.findOrderDetailsById(orderDetailsId);
	}

	@Override
	@Transactional
	public void updateOrderDetails(Orderdetails orderDetails) {
		orderDetailsDAO.updateOrderDetails(orderDetails);
	}

	@Override
	@Transactional
	public void deleteOrderDetails(OrderDetailsId orderDetailsId) {
		orderDetailsDAO.deleteOrderDetails(orderDetailsId);
	}


}
