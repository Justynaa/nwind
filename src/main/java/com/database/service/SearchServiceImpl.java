package com.database.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.SearchDAO;
import com.database.model.SearchForm;

@Service
public class SearchServiceImpl implements SearchService {

	private SearchDAO searchDAO;

	public SearchServiceImpl(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}

	@Override
	@Transactional
	public List<Object[]> search(SearchForm searchForm) {
		return this.searchDAO.search(searchForm);
	}
}
