package com.database.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.ProductsDAO;
import com.database.model.Products;

@Service
public class ProductsServiceImpl implements ProductsService {
	private ProductsDAO productsDAO;

	public ProductsServiceImpl(ProductsDAO productsDAO) {
		this.productsDAO = productsDAO;
	}

	@Override
	@Transactional
	public List<Products> listProducts(int index) {
		return this.productsDAO.listProducts(index);
	}

	@Override
	@Transactional
	public void addProduct(Products product){
		productsDAO.addProduct(product);
	}

	@Override
	@Transactional
	public Products findProductById(int productId) {
		return productsDAO.findProductById(productId);
	}

	@Override
	@Transactional
	public void updateProduct(Products product) {
		productsDAO.updateProduct(product);
	}

	@Override
	@Transactional
	public void deleteProduct(int productId) {
		productsDAO.deleteProduct(productId);
	}
}
