package com.database.service;

import java.util.List;
import com.database.model.Orders;

public interface OrdersService {
	public List<Orders> listOrders(int index);
	public void addOrder(Orders order);
	public Orders findOrderById(int orderId);
	public void updateOrder(Orders order);
	public void deleteOrder(int orderId);
}
