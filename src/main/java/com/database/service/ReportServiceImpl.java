package com.database.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.ReportDAO;

@Service
public class ReportServiceImpl implements ReportService {

	private ReportDAO reportDAO;

	public ReportServiceImpl(ReportDAO reportDAO) {
		this.reportDAO = reportDAO;
	}

	@Override
	@Transactional
	public List<Object[]> report(String reportName) {
		return this.reportDAO.report(reportName);
	}
}
