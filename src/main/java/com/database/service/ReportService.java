package com.database.service;

import java.util.List;

public interface ReportService {
	public List<Object[]> report(String reportName);

}
