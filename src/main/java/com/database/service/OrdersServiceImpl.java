package com.database.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.OrdersDAO;
import com.database.model.Orders;

@Service
public class OrdersServiceImpl implements OrdersService {
	private OrdersDAO ordersDAO;

    public OrdersServiceImpl(OrdersDAO ordersDAO) {
        this.ordersDAO = ordersDAO;
    }
	
	@Override
	@Transactional
	public List<Orders> listOrders(int index) {
		return this.ordersDAO.listOrders(index);
	}
	
	@Override
	@Transactional
	public void addOrder(Orders order){
		ordersDAO.addOrder(order);
	}
	
	@Override
	@Transactional
	public Orders findOrderById(int orderId) {
		return ordersDAO.findOrderById(orderId);
	}
	
	@Override
	@Transactional
	public void updateOrder(Orders order) {
		ordersDAO.updateOrder(order);
	}

	@Override
	@Transactional
	public void deleteOrder(int orderId) {
		ordersDAO.deleteOrder(orderId);
	}


}

