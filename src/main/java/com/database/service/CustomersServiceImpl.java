package com.database.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.CustomersDAO;
import com.database.model.Customers;

@Service
public class CustomersServiceImpl implements CustomersService{
	private CustomersDAO customersDAO;

	public CustomersServiceImpl(CustomersDAO customersDAO) {
		this.customersDAO = customersDAO;
	}

	@Override
	public void indexCustomers() {
		customersDAO.indexCustomers();
	}

	@Override
	@Transactional
	public List<Customers> listCustomers(int index) {
		return this.customersDAO.listCustomers(index);
	}

	@Override
	@Transactional
	public void addCustomer(Customers customer){
		customersDAO.addCustomer(customer);
	}

	@Override
	@Transactional
	public Customers findCustomerById(String customerId) {
		return customersDAO.findCustomerById(customerId);
	}

	@Override
	@Transactional
	public void updateCustomer(Customers customer) {
		customersDAO.updateCustomer(customer);
	}

	@Override
	@Transactional
	public void deleteCustomer(String customerId) {
		customersDAO.deleteCustomer(customerId);
	}

	@Override
	@Transactional
	public List<Customers> searchForCustomer(String searchTerm) {
		return this.customersDAO.searchForCustomer(searchTerm);
	}

}
