package com.database.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.database.model.Orders;
import com.database.service.OrdersService;

@Controller
public class OrdersController {	
	@Autowired
	private OrdersService ordersService;

	@RequestMapping(value="/userPanel/orders/list/{index}", method=RequestMethod.GET)
	public String listOrders(@PathVariable("index") String i, Model model) {
		int index = Integer.parseInt(i);
		
		List<Orders> ordersList = ordersService.listOrders(index);
		model.addAttribute("ordersList", ordersList);
		return "orders";
	}

	//dodawanie nowego zamówienia do bazy
	@RequestMapping(value="/userPanel/orders/add", method=RequestMethod.GET)
	public String addOrder(Model model){
		Orders order = new Orders();
		model.addAttribute("order", order);
		model.addAttribute("edit", false);
		return "editOrder";

	}
	@RequestMapping(value="/userPanel/orders/add", method=RequestMethod.POST)
	public String addOrder(@Valid @ModelAttribute Orders order, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "editOrder";
		}

		ordersService.addOrder(order);
		model.addAttribute("orderId", order.getOrderId());

		return "redirect:/userPanel/orderDetails/{orderId}";
	}

	//Edycja zamówienia
	@RequestMapping(value = { "/userPanel/orders/edit/{orderId}" }, method = RequestMethod.GET)
	public String editOrder(@PathVariable int orderId, ModelMap model) {
		Orders order = ordersService.findOrderById(orderId);
		model.addAttribute("order", order);
		model.addAttribute("edit", true);
		return "editOrder";
	}

	@RequestMapping(value = { "/userPanel/orders/edit/{orderId}" }, method = RequestMethod.POST)
	public String updateOrder(@ModelAttribute("order") Orders order, BindingResult result, ModelMap model,
			@PathVariable int orderId) {

		if (result.hasErrors()) {
			return "editOrder";
		}
		ordersService.updateOrder(order);

		model.addAttribute("success", "Dane zamówienia zostały zaktualizowane.");
		return "success";
	}


	//usuniecie zamówienia
	@RequestMapping(value = "/userPanel/orders/delete/{orderId}", method = RequestMethod.GET)  
	public String deleteOrder(@PathVariable("orderId") int orderId, ModelMap model) {  
		ordersService.deleteOrder(orderId);  
		model.addAttribute("success", "Wybrane zamówienie zostało usunięte.");
		return "success"; 
	}

	//format for dates
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}
}


