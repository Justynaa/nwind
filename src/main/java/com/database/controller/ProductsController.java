package com.database.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.database.model.Products;
import com.database.service.ProductsService;

@Controller
public class ProductsController {	
	@Autowired
	private ProductsService productsService;

	@RequestMapping(value="/userPanel/products/list/{index}", method=RequestMethod.GET)
	public String listProducts(@PathVariable("index") String i, Model model) {
		
		int index = Integer.parseInt(i);
		
		List<Products> productsList = productsService.listProducts(index);
		model.addAttribute("productsList", productsList);
		return "products";
	}

	//dodawanie nowego zam�wienia do bazy
	@RequestMapping(value="/userPanel/products/add", method=RequestMethod.GET)
	public String addProduct(Model model){
		Products product = new Products();
		model.addAttribute("product", product);
		model.addAttribute("edit", false);
		return "editProduct";
	}

	@RequestMapping(value="/userPanel/products/add", method=RequestMethod.POST)
	public String addProduct(@Valid @ModelAttribute("product") Products product, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "editProduct";
		}
		productsService.addProduct(product);
		model.addAttribute("success", "Produkt " + product.getProductName() + " został dodany.");
		return "success";
	}

	//Edycja produktu
	@RequestMapping(value = { "/userPanel/products/edit/{productId}" }, method = RequestMethod.GET)
	public String editProduct(@PathVariable int productId, ModelMap model) {
		Products product = productsService.findProductById(productId);
		model.addAttribute("product", product);
		model.addAttribute("edit", true);
		return "editProduct";
	}

	@RequestMapping(value = { "/userPanel/products/edit/{productId}" }, method = RequestMethod.POST)
	public String updateProduct(@Valid @ModelAttribute("product") Products product, BindingResult result, ModelMap model,
			@PathVariable int productId) {

		if (result.hasErrors()) {
			return "editProduct";
		}
		productsService.updateProduct(product);

		model.addAttribute("success", "Dane produktu " + product.getProductName() + " zostały zaktualizowane.");
		return "success";
	}

	//usuniecie produktu
	@RequestMapping(value = "/userPanel/products/delete/{productId}", method = RequestMethod.GET)  
	public String deleteProduct(@PathVariable("productId") int productId, ModelMap model) {  
		productsService.deleteProduct(productId);  
		model.addAttribute("success", "Wybrany produkt został oznaczony jako wycofany.");
		return "success"; 
	}
}


