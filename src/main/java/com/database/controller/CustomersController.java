package com.database.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.database.model.Customers;
import com.database.service.CustomersService;

@Controller
public class CustomersController {
	@Autowired
	private CustomersService customersService;
	
	//@SuppressWarnings("unchecked")
	@RequestMapping(value = "/userPanel/customers/list/{index}", method = RequestMethod.GET)
	public String listCustomers(@PathVariable("index") String i, Model model) {
		customersService.indexCustomers();
		String searchTerm = "";
		
		int index = Integer.parseInt(i);
		
		List<Customers> customersList = customersService.listCustomers(index);
		model.addAttribute("searchTerm", searchTerm);
		model.addAttribute("customersList", customersList);

		return "customers";
	}
	
	@RequestMapping(value = "/userPanel/customers/list/{index}", method = RequestMethod.POST)
	public String searchForCustomer(@RequestParam("searchTerm") String searchTerm, Model model) {
		List<Customers> customersList = customersService.searchForCustomer(searchTerm);
		model.addAttribute("customersList", customersList);
		return "customers";
	}
	
	// dodawanie nowego klienta do bazy
	@RequestMapping(value = "/userPanel/customers/add", method = RequestMethod.GET)
	public String addCustomer(Model model) {
		Customers customer = new Customers();
		model.addAttribute("customer", customer);
		model.addAttribute("edit", false);
		return "editCustomer";

	}

	@RequestMapping(value = "/userPanel/customers/add", method = RequestMethod.POST)
	public String addCustomer(@Valid @ModelAttribute("customer") Customers customer, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {		
			return "editCustomer";
		}
		customersService.addCustomer(customer);
		model.addAttribute("success", "Klient o nazwie " + customer.getCompanyName() + " został zarejestrowany.");
		return "success";
	}

	// edycja klienta
	@RequestMapping(value = { "/userPanel/customers/edit/{customerId}" }, method = RequestMethod.GET)
	public String editCustomer(@PathVariable String customerId, ModelMap model) {
		Customers customer = customersService.findCustomerById(customerId);
		model.addAttribute("customer", customer);
		model.addAttribute("edit", true);
		return "editCustomer";
	}

	@RequestMapping(value = { "/userPanel/customers/edit/{customerId}" }, method = RequestMethod.POST)
	public String updateCustomer(@Valid @ModelAttribute("customer") Customers customer, BindingResult result, ModelMap model,
			@PathVariable String customerId) {
		if (result.hasErrors()) {
			return "editCustomer";
		}
		
		customersService.updateCustomer(customer);

		model.addAttribute("success", "Dane klienta " + customer.getCompanyName() + " zostały zaktualizowane.");
		return "success";
	}

	// usuniecie klienta
	@RequestMapping(value = "/userPanel/customers/delete/{customerId}", method = RequestMethod.GET)
	public String deleteCustomer(@PathVariable("customerId") String customerId, ModelMap model) {
		customersService.deleteCustomer(customerId);
		model.addAttribute("success", "Wybrany klient został usunięty.");
		return "success";
	}
	
}