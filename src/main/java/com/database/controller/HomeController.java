package com.database.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.database.model.ReportForm;
import com.database.model.SearchForm;
import com.database.service.ReportService;
import com.database.service.SearchService;

@Controller
public class HomeController {
	@Autowired
	private SearchService searchService;

	@Autowired
	private ReportService reportService;

	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
	public String home() {
		return "home";
	}

	// @Secured("hasRole('ROLE_USER')")
	// @PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(value = "/userPanel", method = RequestMethod.GET)
	public String userPanel() {
		return "userPanel";
	}

	// search
	@RequestMapping(value = "/userPanel/search", method = RequestMethod.GET)
	public String search(Model model) {
		SearchForm searchModel = new SearchForm();
		model.addAttribute("searchModel", searchModel);

		return "search";
	}

	@RequestMapping(value = "/userPanel/search", method = RequestMethod.POST)
	public String search(@ModelAttribute("searchModel") SearchForm searchModel, BindingResult result, Model model) {

		List<Object[]> foundList = searchService.search(searchModel);
		model.addAttribute("foundList", foundList);

		if (searchModel.getTypeToSearch().equals("ordDetails")) {
			return "foundOrdDet";
		} else if (searchModel.getTypeToSearch().equals("products")) {
			return "foundProd";
		}

		return "found";
	}

	//report
	@RequestMapping(value = "/userPanel/chooseReport", method = RequestMethod.GET)
	public String chooseReport(Model model) {
		ReportForm reportModel = new ReportForm();
		model.addAttribute("reportModel", reportModel);
		return "chooseReport";
	}

	@RequestMapping(value = "/userPanel/chooseReport", method = RequestMethod.POST)
	public String chooseReport(@ModelAttribute("reportModel") ReportForm reportModel, BindingResult result,
			Model model) {

		if (reportModel.getReportName().equals("totalOrdersCost")) {
			model.addAttribute("reportName", "totalOrdersCost");
		} else if (reportModel.getReportName().equals("totalQuantityInOrders")) {
			model.addAttribute("reportName", "totalQuantityInOrders");
		} else if (reportModel.getReportName().equals("mostOrdered")) {
			model.addAttribute("reportName", "mostOrdered");
		}
		return "redirect:/userPanel/report/{reportName}";
	}

	@RequestMapping(value = "/userPanel/report/{reportName}", method = RequestMethod.GET)
	public String report(@PathVariable("reportName") String reportName, Model model) {

		List<Object[]> reportList = reportService.report(reportName);
		model.addAttribute("reportList", reportList);
		model.addAttribute("reportName", reportName);

		return "report";
	}
}
