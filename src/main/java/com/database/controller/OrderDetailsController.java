package com.database.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.database.model.Orderdetails;
import com.database.model.OrderDetailsId;
import com.database.model.OrderProductForm;
import com.database.model.Orders;
import com.database.model.Products;
import com.database.service.OrderDetailsService;
import com.database.service.OrdersService;
import com.database.service.ProductsService;

@Controller
public class OrderDetailsController {	
	
	@Autowired
	private OrdersService ordersService; 
	@Autowired
	private OrderDetailsService orderDetailsService;
	@Autowired
	private ProductsService productsService;

	@RequestMapping(value="/userPanel/orderDetails/list/{orderId}", method=RequestMethod.GET)
	public String listOrderDetails(Model model, @PathVariable int orderId) {
		Orders order = ordersService.findOrderById(orderId);
		List<Orderdetails> orderDetailsList = orderDetailsService.listOrderDetails(order);
		model.addAttribute("orderDetailsList", orderDetailsList);
		return "orderDetails";
	}
	
	//dodawanie nowego zam�wienia do bazy
	@RequestMapping(value="/userPanel/orderDetails/{orderId}", method=RequestMethod.GET)
	public String addOrderDetails(@PathVariable int orderId, Model model) {
		OrderProductForm opf = new OrderProductForm();
		model.addAttribute("orderProductForm", opf);
		model.addAttribute("edit", false);
		return "editOrderDetails";
	}
	
	@RequestMapping(value="/userPanel/orderDetails/{orderId}", method=RequestMethod.POST)
	public String addOrderDetails(@Valid @ModelAttribute("orderProductForm") OrderProductForm opf,
			BindingResult result, ModelMap model, @PathVariable int orderId) {

		Products product = new Products();
		product = productsService.findProductById(opf.getProducts().getProductId());
		
		if (result.hasErrors()) {
			return "editOrderDetails";
		}
		if (product.isDiscontinued()) {
			model.addAttribute("errMsg", true);
			return "editOrderDetails";
		}

		Orders order = new Orders();
		order = ordersService.findOrderById(orderId);

		Orderdetails orderDetails = new Orderdetails();
		orderDetails.setOrders(order);

		OrderDetailsId id = new OrderDetailsId(orderId, opf.getProducts().getProductId());
		orderDetails.setId(id);
		
		orderDetails.setProducts(opf.getProducts());
		orderDetails.setUnitPrice(opf.getUnitPrice());
		orderDetails.setQuantity(opf.getQuantity());
		orderDetails.setDiscount(opf.getDiscount());

		Set<Orderdetails> odSet = new HashSet<Orderdetails>();
		odSet.add(orderDetails);
		order.setOrderDetailses(odSet);	
		ordersService.updateOrder(order);

		model.addAttribute("orderId", Integer.toString(orderId));
		model.addAttribute("info", "Dodano szczegóły zamówienia produktu o id: " + opf.getProducts().getProductId() + ".");
		return "addedOrdDet";
	}
	
	@RequestMapping(value = { "/userPanel/orderDetails/edit/{orderId}" }, method = RequestMethod.GET)
	public String editOrderDetails(@PathVariable int orderId, ModelMap model) {
		OrderProductForm opf = new OrderProductForm(); 
		model.addAttribute("opf", opf);
		model.addAttribute("edit", true);
		return "editOrderDetails";
	}

	@RequestMapping(value = { "/userPanel/orderDetails/edit/{orderId}" }, method = RequestMethod.POST)
	public String updateOrderDetails(@Valid OrderProductForm opf, BindingResult result, ModelMap model,
			@PathVariable int orderId) {

		if (result.hasErrors()) {
			return "editOrderDetails";
		}
		Orders order = ordersService.findOrderById(orderId);
		
		ordersService.updateOrder(order);

		model.addAttribute("success", "Szczegóły zamówienia " + order.getOrderId() + " zostały zaktualizowane.");
		return "success";
	}
}

