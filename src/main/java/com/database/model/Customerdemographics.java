package com.database.model;
// Generated 2016-11-08 19:58:36 by Hibernate Tools 5.2.0.Beta1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Customerdemographics generated by hbm2java
 */
@Entity
@Table(name = "customerdemographics", catalog = "northwind")
public class Customerdemographics implements java.io.Serializable {
	
	private String customerTypeId;
	private String customerDesc;
	private Set<Customers> customerses = new HashSet<Customers>(0);

	public Customerdemographics() {
	}

	public Customerdemographics(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public Customerdemographics(String customerTypeId, String customerDesc, Set<Customers> customerses) {
		this.customerTypeId = customerTypeId;
		this.customerDesc = customerDesc;
		this.customerses = customerses;
	}

	@Id
	@Column(name = "CustomerTypeID", unique = true, nullable = false, length = 10)
	public String getCustomerTypeId() {
		return this.customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	@Column(name = "CustomerDesc", length = 16777215)
	public String getCustomerDesc() {
		return this.customerDesc;
	}

	public void setCustomerDesc(String customerDesc) {
		this.customerDesc = customerDesc;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "customerdemographicses")
	public Set<Customers> getCustomerses() {
		return this.customerses;
	}

	public void setCustomerses(Set<Customers> customerses) {
		this.customerses = customerses;
	}

}
