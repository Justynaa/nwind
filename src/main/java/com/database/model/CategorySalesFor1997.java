package com.database.model;
// Generated 2016-11-08 19:58:36 by Hibernate Tools 5.2.0.Beta1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * CategorySalesFor1997 generated by hbm2java
 */
@Entity
@Table(name = "category sales for 1997", catalog = "northwind")
public class CategorySalesFor1997 implements java.io.Serializable {

	private CategorySalesFor1997Id id;

	public CategorySalesFor1997() {
	}

	public CategorySalesFor1997(CategorySalesFor1997Id id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "categoryName", column = @Column(name = "CategoryName", nullable = false, length = 15)),
			@AttributeOverride(name = "categorySales", column = @Column(name = "CategorySales", precision = 25, scale = 8)) })
	public CategorySalesFor1997Id getId() {
		return this.id;
	}

	public void setId(CategorySalesFor1997Id id) {
		this.id = id;
	}

}
