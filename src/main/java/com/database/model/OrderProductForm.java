package com.database.model;

import java.math.BigDecimal;

public class OrderProductForm {
	private Products products;
	private BigDecimal unitPrice;
	private short quantity;
	private double discount;

	public OrderProductForm() {
	}

	public OrderProductForm(Products products, BigDecimal unitPrice, short quantity, double discount) {
		this.products = products;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
		this.discount = discount;
	}

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public short getQuantity() {
		return quantity;
	}

	public void setQuantity(short quantity) {
		this.quantity = quantity;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

}
