package com.database.model;
// Generated 2016-11-08 19:58:36 by Hibernate Tools 5.2.0.Beta1

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AlphabeticalListOfProductsId generated by hbm2java
 */
@Embeddable
public class AlphabeticalListOfProductsId implements java.io.Serializable {

	private int productId;
	private String productName;
	private Integer supplierId;
	private Integer categoryId;
	private String quantityPerUnit;
	private BigDecimal unitPrice;
	private Short unitsInStock;
	private Short unitsOnOrder;
	private Short reorderLevel;
	private boolean discontinued;
	private String categoryName;

	public AlphabeticalListOfProductsId() {
	}

	public AlphabeticalListOfProductsId(int productId, String productName, boolean discontinued, String categoryName) {
		this.productId = productId;
		this.productName = productName;
		this.discontinued = discontinued;
		this.categoryName = categoryName;
	}

	public AlphabeticalListOfProductsId(int productId, String productName, Integer supplierId, Integer categoryId,
			String quantityPerUnit, BigDecimal unitPrice, Short unitsInStock, Short unitsOnOrder, Short reorderLevel,
			boolean discontinued, String categoryName) {
		this.productId = productId;
		this.productName = productName;
		this.supplierId = supplierId;
		this.categoryId = categoryId;
		this.quantityPerUnit = quantityPerUnit;
		this.unitPrice = unitPrice;
		this.unitsInStock = unitsInStock;
		this.unitsOnOrder = unitsOnOrder;
		this.reorderLevel = reorderLevel;
		this.discontinued = discontinued;
		this.categoryName = categoryName;
	}

	@Column(name = "ProductID", nullable = false)
	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Column(name = "ProductName", nullable = false, length = 40)
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "SupplierID")
	public Integer getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	@Column(name = "CategoryID")
	public Integer getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "QuantityPerUnit", length = 20)
	public String getQuantityPerUnit() {
		return this.quantityPerUnit;
	}

	public void setQuantityPerUnit(String quantityPerUnit) {
		this.quantityPerUnit = quantityPerUnit;
	}

	@Column(name = "UnitPrice", precision = 10, scale = 4)
	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Column(name = "UnitsInStock")
	public Short getUnitsInStock() {
		return this.unitsInStock;
	}

	public void setUnitsInStock(Short unitsInStock) {
		this.unitsInStock = unitsInStock;
	}

	@Column(name = "UnitsOnOrder")
	public Short getUnitsOnOrder() {
		return this.unitsOnOrder;
	}

	public void setUnitsOnOrder(Short unitsOnOrder) {
		this.unitsOnOrder = unitsOnOrder;
	}

	@Column(name = "ReorderLevel")
	public Short getReorderLevel() {
		return this.reorderLevel;
	}

	public void setReorderLevel(Short reorderLevel) {
		this.reorderLevel = reorderLevel;
	}

	@Column(name = "Discontinued", nullable = false)
	public boolean isDiscontinued() {
		return this.discontinued;
	}

	public void setDiscontinued(boolean discontinued) {
		this.discontinued = discontinued;
	}

	@Column(name = "CategoryName", nullable = false, length = 15)
	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AlphabeticalListOfProductsId))
			return false;
		AlphabeticalListOfProductsId castOther = (AlphabeticalListOfProductsId) other;

		return (this.getProductId() == castOther.getProductId())
				&& ((this.getProductName() == castOther.getProductName())
						|| (this.getProductName() != null && castOther.getProductName() != null
								&& this.getProductName().equals(castOther.getProductName())))
				&& ((this.getSupplierId() == castOther.getSupplierId()) || (this.getSupplierId() != null
						&& castOther.getSupplierId() != null && this.getSupplierId().equals(castOther.getSupplierId())))
				&& ((this.getCategoryId() == castOther.getCategoryId()) || (this.getCategoryId() != null
						&& castOther.getCategoryId() != null && this.getCategoryId().equals(castOther.getCategoryId())))
				&& ((this.getQuantityPerUnit() == castOther.getQuantityPerUnit())
						|| (this.getQuantityPerUnit() != null && castOther.getQuantityPerUnit() != null
								&& this.getQuantityPerUnit().equals(castOther.getQuantityPerUnit())))
				&& ((this.getUnitPrice() == castOther.getUnitPrice()) || (this.getUnitPrice() != null
						&& castOther.getUnitPrice() != null && this.getUnitPrice().equals(castOther.getUnitPrice())))
				&& ((this.getUnitsInStock() == castOther.getUnitsInStock())
						|| (this.getUnitsInStock() != null && castOther.getUnitsInStock() != null
								&& this.getUnitsInStock().equals(castOther.getUnitsInStock())))
				&& ((this.getUnitsOnOrder() == castOther.getUnitsOnOrder())
						|| (this.getUnitsOnOrder() != null && castOther.getUnitsOnOrder() != null
								&& this.getUnitsOnOrder().equals(castOther.getUnitsOnOrder())))
				&& ((this.getReorderLevel() == castOther.getReorderLevel())
						|| (this.getReorderLevel() != null && castOther.getReorderLevel() != null
								&& this.getReorderLevel().equals(castOther.getReorderLevel())))
				&& (this.isDiscontinued() == castOther.isDiscontinued())
				&& ((this.getCategoryName() == castOther.getCategoryName())
						|| (this.getCategoryName() != null && castOther.getCategoryName() != null
								&& this.getCategoryName().equals(castOther.getCategoryName())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getProductId();
		result = 37 * result + (getProductName() == null ? 0 : this.getProductName().hashCode());
		result = 37 * result + (getSupplierId() == null ? 0 : this.getSupplierId().hashCode());
		result = 37 * result + (getCategoryId() == null ? 0 : this.getCategoryId().hashCode());
		result = 37 * result + (getQuantityPerUnit() == null ? 0 : this.getQuantityPerUnit().hashCode());
		result = 37 * result + (getUnitPrice() == null ? 0 : this.getUnitPrice().hashCode());
		result = 37 * result + (getUnitsInStock() == null ? 0 : this.getUnitsInStock().hashCode());
		result = 37 * result + (getUnitsOnOrder() == null ? 0 : this.getUnitsOnOrder().hashCode());
		result = 37 * result + (getReorderLevel() == null ? 0 : this.getReorderLevel().hashCode());
		result = 37 * result + (this.isDiscontinued() ? 1 : 0);
		result = 37 * result + (getCategoryName() == null ? 0 : this.getCategoryName().hashCode());
		return result;
	}

}
