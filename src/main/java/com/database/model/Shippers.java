package com.database.model;
// Generated 2016-11-08 19:58:36 by Hibernate Tools 5.2.0.Beta1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Shippers generated by hbm2java
 */
@Entity
@Table(name = "shippers", catalog = "northwind")
public class Shippers implements java.io.Serializable {
	
	private Integer shipperId;
	private String companyName;
	private String phone;
	private Set<Orders> orderses = new HashSet<Orders>(0);

	public Shippers() {
	}

	public Shippers(String companyName) {
		this.companyName = companyName;
	}

	public Shippers(String companyName, String phone, Set<Orders> orderses) {
		this.companyName = companyName;
		this.phone = phone;
		this.orderses = orderses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ShipperID", unique = true, nullable = false)
	public Integer getShipperId() {
		return this.shipperId;
	}

	public void setShipperId(Integer shipperId) {
		this.shipperId = shipperId;
	}

	@Column(name = "CompanyName", nullable = false, length = 40)
	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "Phone", length = 24)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shippers")
	public Set<Orders> getOrderses() {
		return this.orderses;
	}

	public void setOrderses(Set<Orders> orderses) {
		this.orderses = orderses;
	}

}
