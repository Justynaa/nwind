package com.database.model;

public class ReportForm {
	
	private Customers customers;
	private Products products;
	private String reportName, reportCategory;
	
	public ReportForm() {
	}

	public ReportForm(Customers customers, Products products, String reportName) {
		this.customers = customers;
		this.products = products;
		this.reportName = reportName;
	}
	
	public Customers getCustomers() {
		return customers;
	}
	
	public void setCustomers(Customers customers) {
		this.customers = customers;
	}

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	public String getReportCategory() {
		return reportCategory;
	}

	public void setReportCategory(String reportCategory) {
		this.reportCategory = reportCategory;
	}


}
