package com.database.model;

public class SearchForm {
	
	private Customers customers;
	private Products products;
	private String typeToSearch;
	
	public SearchForm() {
	}

	public SearchForm(Customers customers, Products products) {
		this.customers = customers;
		this.products = products;
	}

	public Customers getCustomers() {
		return customers;
	}

	public void setCustomers(Customers customers) {
		this.customers = customers;
	}
	
	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}
	
	public String getTypeToSearch() {
		return typeToSearch;
	}

	public void setTypeToSearch(String typeToSearch) {
		this.typeToSearch = typeToSearch;
	}

}
