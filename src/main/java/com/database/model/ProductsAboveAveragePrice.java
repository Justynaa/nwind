package com.database.model;
// Generated 2016-11-08 19:58:36 by Hibernate Tools 5.2.0.Beta1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ProductsAboveAveragePrice generated by hbm2java
 */
@Entity
@Table(name = "products above average price", catalog = "northwind")
public class ProductsAboveAveragePrice implements java.io.Serializable {

	private ProductsAboveAveragePriceId id;

	public ProductsAboveAveragePrice() {
	}

	public ProductsAboveAveragePrice(ProductsAboveAveragePriceId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "productName", column = @Column(name = "ProductName", nullable = false, length = 40)),
			@AttributeOverride(name = "unitPrice", column = @Column(name = "UnitPrice", precision = 10, scale = 4)) })
	public ProductsAboveAveragePriceId getId() {
		return this.id;
	}

	public void setId(ProductsAboveAveragePriceId id) {
		this.id = id;
	}

}
