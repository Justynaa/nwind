package com.database.model;
// Generated 2016-11-08 19:58:36 by Hibernate Tools 5.2.0.Beta1

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ProductSalesFor1997Id generated by hbm2java
 */
@Embeddable
public class ProductSalesFor1997Id implements java.io.Serializable {

	private String categoryName;
	private String productName;
	private Double productSales;

	public ProductSalesFor1997Id() {
	}

	public ProductSalesFor1997Id(String categoryName, String productName) {
		this.categoryName = categoryName;
		this.productName = productName;
	}

	public ProductSalesFor1997Id(String categoryName, String productName, Double productSales) {
		this.categoryName = categoryName;
		this.productName = productName;
		this.productSales = productSales;
	}

	@Column(name = "CategoryName", nullable = false, length = 15)
	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Column(name = "ProductName", nullable = false, length = 40)
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "ProductSales", precision = 25, scale = 8)
	public Double getProductSales() {
		return this.productSales;
	}

	public void setProductSales(Double productSales) {
		this.productSales = productSales;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProductSalesFor1997Id))
			return false;
		ProductSalesFor1997Id castOther = (ProductSalesFor1997Id) other;

		return ((this.getCategoryName() == castOther.getCategoryName()) || (this.getCategoryName() != null
				&& castOther.getCategoryName() != null && this.getCategoryName().equals(castOther.getCategoryName())))
				&& ((this.getProductName() == castOther.getProductName())
						|| (this.getProductName() != null && castOther.getProductName() != null
								&& this.getProductName().equals(castOther.getProductName())))
				&& ((this.getProductSales() == castOther.getProductSales())
						|| (this.getProductSales() != null && castOther.getProductSales() != null
								&& this.getProductSales().equals(castOther.getProductSales())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getCategoryName() == null ? 0 : this.getCategoryName().hashCode());
		result = 37 * result + (getProductName() == null ? 0 : this.getProductName().hashCode());
		result = 37 * result + (getProductSales() == null ? 0 : this.getProductSales().hashCode());
		return result;
	}

}
