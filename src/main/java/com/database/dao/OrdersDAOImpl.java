package com.database.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.database.model.Orders;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

@Repository
public class OrdersDAOImpl implements OrdersDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> listOrders(int index) {
		Session session = this.sessionFactory.getCurrentSession();

		List<Orders> ordersList = null;
		if(index == 1) {
			ordersList = session.createQuery("from Orders")
					.setFirstResult(index-1)
					.setMaxResults(index+24)
					.list();
		} else if (index > 1) {
			int n = index;
			index = 25*(n-1);
			ordersList = session.createQuery("from Orders")
					.setFirstResult(index)
					.setMaxResults(index*n-1)
					.list();
		}
		return ordersList;
	}

	@Override
	public void addOrder(Orders order){
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(order);
	}

	@Override
	public Orders findOrderById(int orderId) {
		Session session = this.sessionFactory.getCurrentSession();
		Orders order = (Orders) session.get(Orders.class, orderId);
		return order;
	}

	@Override
	public void updateOrder(Orders order) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(order);
	}

	@Override
	public void deleteOrder(int orderId){
		Session session = this.sessionFactory.getCurrentSession();
		Orders order = (Orders) session.get(Orders.class, orderId);
		if(null != order){
			session.delete(order);
		}
	}
}
