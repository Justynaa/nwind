package com.database.dao;

import java.util.List;

import com.database.model.Products;

public interface ProductsDAO {
	public List<Products> listProducts(int index);
	public void addProduct(Products product);
	public Products findProductById(int productId);
	public void updateProduct(Products product);
	public void deleteProduct(int productId);
}
