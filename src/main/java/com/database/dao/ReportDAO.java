package com.database.dao;

import java.util.List;

public interface ReportDAO {
	public List<Object[]> report(String reportName);

}
