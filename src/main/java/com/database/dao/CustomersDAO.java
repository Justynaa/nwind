package com.database.dao;

import java.util.List;

import com.database.model.Customers;

public interface CustomersDAO {
	public void indexCustomers();
	public List<Customers> listCustomers(int index);
	public void addCustomer(Customers customer);
	public Customers findCustomerById(String customerId);
	public void updateCustomer(Customers customer);
	public void deleteCustomer(String customerId);
	public List<Customers> searchForCustomer(String searchTerm);
}
