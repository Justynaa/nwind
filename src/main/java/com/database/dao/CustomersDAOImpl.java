package com.database.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.database.model.Customers;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.search.FullTextSession;

@Repository
public class CustomersDAOImpl implements CustomersDAO {
	@Autowired
	private SessionFactory sessionFactory;
	private Session session;
	private FullTextSession fTS;

	@Override
	public void indexCustomers(){
		try {
			session = sessionFactory.openSession();

			fTS = Search.getFullTextSession(session);
			fTS.createIndexer().startAndWait();
		}
		catch(InterruptedException ie) {

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customers> listCustomers(int index) {
		session = this.sessionFactory.getCurrentSession();

		List<Customers> customersList = null;
		if(index == 1) {
			customersList = session.createQuery("from Customers")
					.setFirstResult(index-1)
					.setMaxResults(index+24)
					.list();
		} else if (index > 1) {
			int n = index;
			index = 25*(n-1);
			customersList = session.createQuery("from Customers")
					.setFirstResult(index)
					.setMaxResults(index*n-1)
					.list();
		}
		return customersList;
	}

	@Override
	public void addCustomer(Customers customer){
		session = this.sessionFactory.getCurrentSession();
		session.persist(customer);
	}

	@Override
	public Customers findCustomerById(String customerId) {
		session = this.sessionFactory.getCurrentSession();
		Customers customer = (Customers) session.get(Customers.class, customerId);
		return customer;
	}

	@Override
	public void updateCustomer(Customers customer) {
		session = this.sessionFactory.getCurrentSession();
		session.update(customer);
	}

	@Override
	public void deleteCustomer(String customerId){
		session = this.sessionFactory.getCurrentSession();
		Customers customer = (Customers) session.get(Customers.class, customerId);
		if(null != customer){
			session.delete(customer);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Customers> searchForCustomer(String searchTerm) {
		try {
			session = sessionFactory.getCurrentSession();

			fTS = Search.getFullTextSession(session);

			QueryBuilder qb = fTS.getSearchFactory()
					.buildQueryBuilder()
					.forEntity(Customers.class)
					.get();
			org.apache.lucene.search.Query query = qb
					.keyword()
					.onFields("companyName", "country")
					.matching(searchTerm)
					.createQuery();

			org.hibernate.Query hQuery = (Query) fTS
					.createFullTextQuery(query, Customers.class);

			List<Customers> results = (List<Customers>) hQuery.list();

			return results;
		}
		catch(Exception e) {
			throw e;
		}
	}

}
