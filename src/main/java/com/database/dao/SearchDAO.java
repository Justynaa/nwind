package com.database.dao;

import java.util.List;

import com.database.model.SearchForm;

public interface SearchDAO {
	public List<Object[]> search(SearchForm searchForm);

}
