package com.database.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.database.model.Orderdetails;
import com.database.model.Orders;
import com.database.model.OrderDetailsId;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

@Repository
public class OrderDetailsDAOImpl implements OrderDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Orderdetails> listOrderDetails(Orders order) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Orderdetails> orderDetailsList = null;
		orderDetailsList = session.createQuery("from Orderdetails od where od.orders = :order")
				.setParameter("order", order)
				.list();
		return orderDetailsList;
	}

	@Override
	public Orderdetails findOrderDetailsById(OrderDetailsId orderDetailsId) {
		Session session = this.sessionFactory.getCurrentSession();
		Orderdetails orderDetails = (Orderdetails) session.get(Orderdetails.class, orderDetailsId);
		return orderDetails;
	}

	@Override
	public void updateOrderDetails(Orderdetails orderDetails) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(orderDetails);
	}

	@Override
	public void deleteOrderDetails(OrderDetailsId orderDetailsId){
		Session session = this.sessionFactory.getCurrentSession();
		Orderdetails orderDetails = (Orderdetails) session.get(Orderdetails.class, orderDetailsId);
		if(null != orderDetails){
			session.delete(orderDetails);
		}
	}

}
