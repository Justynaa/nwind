package com.database.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReportDAOImpl implements ReportDAO {
	@Autowired
	private SessionFactory sessionFactory;
	private Session session;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> report(String reportName) {
		session = this.sessionFactory.getCurrentSession();

		List<Object[]> reportList = null;
		String query = null;

		if (reportName.equals("totalOrdersCost")) {
			query = "select c.customerId, c.companyName, SUM(od.unitPrice*od.quantity*(1-od.discount)) "
					+ "from Orderdetails od "
					+ "inner join od.orders o "
					+ "inner join o.customers c "
					+ "group by c.customerId "
					+ "order by sum(od.unitPrice*od.quantity*(1-od.discount)) desc";
			reportList = session.createQuery(query)
					.setFirstResult(0)
					.setMaxResults(10)
					.list();
		} else if (reportName.equals("totalQuantityInOrders")) {
			query = "select p.productName, sum(od.quantity) from Orderdetails od "
					+ "inner join od.products p "
					+ "group by p.productName "
					+ "order by sum(od.quantity) desc ";
			reportList = session.createQuery(query)
					.setFirstResult(0)
					.setMaxResults(10)
					.list();
		} else if (reportName.equals("mostOrdered")) {
			query = "select p.productName, count(od.orders) from Orderdetails od "
					+ "inner join od.products p "
					+ "group by p.productName "
					+ "order by sum(od.orders) desc";
			reportList = session.createQuery(query)
					.setFirstResult(0)
					.setMaxResults(10)
					.list();
		}

		return reportList;
	}	
}
