package com.database.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.database.model.Products;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

@Repository
public class ProductsDAOImpl implements ProductsDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Products> listProducts(int index) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Products> productsList = null;
		if(index == 1) {
			productsList = session.createQuery("from Products")
					.setFirstResult(index-1)
					.setMaxResults(index+39)
					.list();
		} else if (index > 1) {
			index = 40;
			productsList = session.createQuery("from Products")
					.setFirstResult(index)
					.setMaxResults(index*2)
					.list();
		}
		return productsList;
	}

	@Override
	public void addProduct(Products product){
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(product);
	}

	@Override
	public Products findProductById(int productId) {
		Session session = this.sessionFactory.getCurrentSession();
		Products product = (Products) session.get(Products.class, productId);
		return product;
	}

	@Override
	public void updateProduct(Products product) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(product);
	}

	@Override
	public void deleteProduct(int productId){
		Session session = this.sessionFactory.getCurrentSession();
		Products product = (Products) session.get(Products.class, productId);
		if(null != product){
			session.createQuery("update Products p set p.discontinued = 1 where p.productId = :id")
			.setParameter("id", productId)
			.executeUpdate();

		}
	}
}
