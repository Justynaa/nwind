package com.database.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.database.model.SearchForm;

@Repository
public class SearchDAOImpl implements SearchDAO {
	@Autowired
	private SessionFactory sessionFactory;
	private Session session;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> search(SearchForm searchForm) {
		session = this.sessionFactory.getCurrentSession();

		List<Object[]> foundList = null;
		String query = null;

		String isChecked = searchForm.getTypeToSearch();
		String productName = searchForm.getProducts().getProductName();
		String companyName = searchForm.getCustomers().getCompanyName();
		
		if(!productName.isEmpty() && isChecked.equals("customers")) {
			query = "select c.customerId, c.companyName, od.orders from Orderdetails od "
					+ "inner join od.products p "
					+ "inner join od.orders o "
					+ "inner join o.customers c "
					+ "where p.productName = :productName "
					+ "order by c.customerId";
			foundList = session.createQuery(query)
					.setParameter("productName", productName)
					.list();
		} else if(!productName.isEmpty() && companyName.isEmpty() && isChecked.equals("ordDetails")) {
			query = "select od.orders, od.products, od.quantity from Orderdetails od "
					+ "inner join od.products p "
					+ "where p.productName = :productName "
					+ "order by od.orders";
			foundList = session.createQuery(query)
					.setParameter("productName", productName)
					.list();
		} else if(productName.isEmpty() && !companyName.isEmpty() && isChecked.equals("ordDetails")) {
			query = "select od.orders, od.products, od.quantity from Orderdetails od "
					+ "inner join od.orders o "
					+ "inner join o.customers c "
					+ "where c.companyName = :companyName";
			foundList = session.createQuery(query)
					.setParameter("companyName", companyName)
					.list();
		} else if(!productName.isEmpty() && !companyName.isEmpty() && isChecked.equals("ordDetails")) {
			query = "select od.orders, od.products, od.quantity from Orderdetails od "
					+ "inner join od.products p "
					+ "inner join od.orders o "
					+ "inner join o.customers c "
					+ "where c.companyName = :companyName "
					+ "and p.productName = :productName";
			foundList = session.createQuery(query)
					.setParameter("companyName", companyName)
					.setParameter("productName", productName)
					.list();
		} else if(productName.isEmpty() && !companyName.isEmpty() && isChecked.equals("products")) {
			query = "select p.productId, p.productName, od.orders from Orderdetails od "
					+ "inner join od.products p "
					+ "inner join od.orders o "
					+ "inner join o.customers c "
					+ "where c.companyName = :companyName";
			foundList = session.createQuery(query)
					.setParameter("companyName", companyName)
					.list();
		} 

		return foundList;
	}	
}
