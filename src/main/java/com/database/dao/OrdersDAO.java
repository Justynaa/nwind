package com.database.dao;

import java.util.List;

import com.database.model.Orders;

public interface OrdersDAO {
	public List<Orders> listOrders(int index);
	public void addOrder(Orders order);
	public Orders findOrderById(int orderId);
	public void updateOrder(Orders order);
	public void deleteOrder(int orderId);
}

